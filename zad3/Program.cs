using System;
using System.Collections;
using System.Collections.Generic;

namespace RPPOON_LV4_2_AP
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IRentable> renting = new List<IRentable>();
            IRentable book = new Book("Book of shadows");
            IRentable video = new Video("Video: game");
            renting.Add(book);
            renting.Add(video);
            RentingConsolePrinter printer = new RentingConsolePrinter();
            printer.DisplayItems(renting);
            printer.PrintTotalPrice(renting);


        }
    }
}
