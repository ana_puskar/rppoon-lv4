using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON_LV4_2_AP
{
    interface IRentable
    {
        String Description { get; }
        double CalculatePrice();

    }
}
