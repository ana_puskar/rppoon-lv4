using System;
using System.Collections;
using System.Collections.Generic;

namespace RPPOON_LV4_2_AP
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IRentable> renting = new List<IRentable>();
            IRentable book = new Book("Book of shadows");
            IRentable video = new Video("Video: game");
            renting.Add(book);
            renting.Add(video);
            RentingConsolePrinter printer = new RentingConsolePrinter();

            IRentable newBook = new Book("Book of goblins");
            IRentable newVideo = new Video("Home: video");
            IRentable hotItem1 = new HotItem(newBook);
            IRentable hotItem2 = new HotItem(newVideo);
            renting.Add(hotItem1);
            renting.Add(hotItem2);
            printer.DisplayItems(renting);
            printer.PrintTotalPrice(renting);

            List<IRentable> flashSale = new List<IRentable>();
            DiscountedItem discountedBook = new DiscountedItem(book, 50);
            DiscountedItem discountedVideo = new DiscountedItem(video, 70);
            DiscountedItem discountedHotItem1 = new DiscountedItem(hotItem1, 20);
            DiscountedItem discountedHotItem2 = new DiscountedItem(hotItem2, 30);
            flashSale.Add(discountedBook);
            flashSale.Add(discountedVideo);
            flashSale.Add(discountedHotItem1);
            flashSale.Add(discountedHotItem2);
            printer.DisplayItems(flashSale);
            printer.PrintTotalPrice(flashSale);
        }
    }
}
