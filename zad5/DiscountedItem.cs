using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON_LV4_2_AP
{
    class DiscountedItem : RentableDecorator
    {
        private readonly double discountPercentage;
        public DiscountedItem(IRentable rentable, double discount) : base(rentable) 
        {
            this.discountPercentage = discount;
        }
        public override double CalculatePrice()
        {
            return base.CalculatePrice() - this.discountPercentage/100 * base.CalculatePrice();
        }
        public override String Description
        {
            get
            {
                return base.Description + " now at " + this.discountPercentage + "% off!";
            }
        }
    }
}