using System;
using System.Collections;
using System.Collections.Generic;

namespace RPPOON_LV4_2_AP
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IRentable> renting = new List<IRentable>();
            IRentable book = new Book("Book of shadows");
            IRentable video = new Video("Video: game");
            renting.Add(book);
            renting.Add(video);
            RentingConsolePrinter printer = new RentingConsolePrinter();

            IRentable newBook = new Book("Book of goblins");
            IRentable newVideo = new Video("Home: video");
            IRentable hotItem1 = new HotItem(newBook);
            IRentable hotItem2 = new HotItem(newVideo);
            renting.Add(hotItem1);
            renting.Add(hotItem2);
            printer.DisplayItems(renting);
            printer.PrintTotalPrice(renting);

        }
    }
}